package database.models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <pre>
 * Ova klasa enkapsulira jedan entitet u bazi podataka. Efektivno radi jednu tablicu u bazi podataka sa nazivom "dogs".
 * </pre>
 * 
 * @author hrvoje
 *
 */
@Entity(name = "dogs") /*
						 * Ova anotacija označava da je ovaj objekt "kalup" za tablicu u bazi podataka,
						 * parametar name je opcionalan ako se ne navede dodjelit će se tablici ime dog
						 * (nisam ziher tocno koje ime al dodjelit ce se neko ime sigurno)
						 */

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder /*
			 * Data, Builder, NoArgsContructor i AllArgsConstructor anotacija NEMAJU veze sa bazom podataka. Ove anotacije dolaze
			 * iz dependecija lombok. Lombok dependecy je zaduzen da generira tzv.
			 * boilder-plate code odnosno suvisan kod. Tako recimo @Data će automatski pri
			 * kompalaciji dodati getere, setere, kontruktor sa svim argumentima, hash code,
			 * equals i toString metode. S druge strane anotacija Builder će napraviti
			 * statičku metodu nad klasom Dog naziva builder. To nam omogučuje da pišemo kod
			 * npr: Dog dog = Dog.builder().name("Pero").age(10).build();
			 * AllArgsCOntructor i NoArgsConstructor naprave samo konstruktore sa svim argumentima i sa 0 argumenata.
			 */
public class Dog {

	@Id
	/*
	 * Id anotacija označava da će ovo polje(private long id) biti id u tablici u
	 * bazi podataka. To što je to polje ovdje nazvano id ne znači ništa, moglo se
	 * zvati kako god. Time što je ovo polje označeno kao id bit će jedinstveno u
	 * tablici.
	 */
	@GeneratedValue(strategy = GenerationType.AUTO)
	/*
	 * Anotacija GeneratedValue označava da nećemo mi davat vrijednosti ovom objektu
	 * već će prilikom spremanja u bazu objekt dobiti svoj id, tako da ubiti kad god
	 * stvaramo objekt tipa dog nikad mu nedajemo parametar id, na žalost lombok
	 * nema anotaciju da onemogučimo inicijalizaicju polja pa nisam to mogao
	 * onemogučit.
	 */
	private long id;

	@NotEmpty // Anotacija nalaže da ovo polje ne smije biti prazno, inače će doći do greške
	@NotNull // Anotacija nalaže da ovo polje ne smije biti null, inače će doći do greške
	private String name;

	@NotNull
	private int age;

	@ManyToOne(fetch = FetchType.LAZY)
	/*
	 * Ova anotacija označava povezanost između objekata. Čitaj ovaj objekt(Dog) ima
	 * jednog vlasnika(Person), ali jedan vlasnik(Person) ima više pasa(Dog).
	 * Postoje i druge anotacija @OneToMany, @OneToOne i @ManyToMany
	 */
	private Person owner;
}
