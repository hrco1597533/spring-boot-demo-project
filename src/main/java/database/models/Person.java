package database.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <pre>
 * POGLEDAJ {@code database.models.Dog} ZA OBJAŠNJENJE SVIH ANOTACIJA.
 * </pre>
 * @author hrvoje
 *
 */

@Entity(name = "persons")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Person {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotEmpty
	@NotNull
	private String name;
	
	@NotNull
	private int age;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "owner")
	private List<Dog> dogs;
	
}
