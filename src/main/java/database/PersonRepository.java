package database;

import org.springframework.data.jpa.repository.JpaRepository;

import database.models.Person;

/**
 * <pre>
 * POGLEDAJ {@link database.DogRepository}
 * </pre>
 * 
 * @author hrvoje
 *
 */
public interface PersonRepository extends JpaRepository<Person, Long> {}
