package database;

import org.springframework.data.jpa.repository.JpaRepository;

import database.models.Dog;

/**
 * <pre>
 * Ovo je jedan od repositorija(drugi je PersonRepository) koji se korisiti u ovom demo projektu. Ako do sad niste primjetili svugdje
 * u kodu se referenca na ovo sučelje koristi najnoramlnije kao da je već implementirano, a kao što vidit ovdje nema nikakvog koda.
 * Također ne ćete vidjeti nigdje ni jedan razred koji implementira ovo sučelje, to je zato jer spring izgenerira implementaciju ovog
 * sučelja. Izgenerira osnovne funkcionalnosti ovog sučelja kao što su save, delete, find... Dakle da, ne moramo ništa od toga implementirati
 * te sve funkcionalnosti dobivamo za džabe. Spring napravi sve tablice u bazi podataka i implementira sučelje preko kojeg možemo spremati i
 * dohvačati podatke iz baze podataka.
 * </pre>
 * @author hrvoje
 *
 */
public interface DogsReporsitory extends JpaRepository<Dog, Long> {}
