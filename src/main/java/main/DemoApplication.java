package main;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import database.DogsReporsitory;
import database.PersonRepository;
import database.models.Dog;
import database.models.Person;

/**
 * <pre>
 * Ovo je ulazna točka. Da bi se spring uključio uopče treba ga upaliti kroz main metodu. Uglavnom kada se pozove
 * metoda SpringAplication.run(...) u osnovi se "upali" spring framework.
 * </pre>
 * 
 * @author hrvoje
 *
 */
@SpringBootApplication
	/*
	 * Pošto koristimo spring boot, a ne čisti spring ova anotacija(SpringBootApplication) nam pali neke preddefinirane postavke. Kao recimo
	 * da kada upišete localhost:8080 u svoj browser da automatski traži index.html u src/main/resources/static inače bi ovo trebali manualno
	 * podešavati.
	 */
@EntityScan(basePackages = {"database.models"})
	/*
	 * Pokušat ću slikovito objasniti ovu anotaciju. Primjetite kako se ova klasa nalazi u main paketu. Kada bi naša cijela aplikacija bila u main paketu,
	 * dakle recimo da umjesto da imamo database paket izvan main paketa imamo main.database paket, onda ova anotacija nebi bila potrebna. Ono što ova anotacija
	 * radi je to da skenira one pakete koje joj date pod parametrom basePackages i traži sve modele. Modeli su sve klase koje su anotirane sa anotacijom @Entity
	 * (pogledaj klase Dog i Person). Jedom kad spring nađe te razrede tada će moći inicijalizirati bazu podataka sa tim entitetima.
	 */
@EnableJpaRepositories(basePackages = {"database"})
	/*
	 * Također ista stvar kao i sa @EntityScan-om, samo ova anotacija traži bilo koje razrede koji naslijeđuju JpaRepository interface(vidi PersonRepository i DogRepository).
	 * Kada database paket nebi bio izvan main paketa tada ovo ne bi trebali pisati jer bi se automatski pretražio cijeli main paket.
	 */
@ComponentScan(basePackages = {"web", "database"})
	/*
	 * Također ista stvar kao predhodna dva scanna, ali ovaj sken traži sve razrede koji su anotirani sa anotacijom @Component ili nekom od izvedenica kao recimo
	 * @Repository, @Service, @Controller, @RestController...
	 */
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	/*
	 * Za @Autowired anotaciju pogledajte README.txt
	 */
	@Autowired
	private DogsReporsitory dogRepo;
	
	@Autowired
	private PersonRepository personRepo;
	
	@PostConstruct
	/*
	 * @PostConstruct anotacija označava metodu koju će spring izvršiti kada izgradi bean DemoApplication. Ako ne znaš što su bean-ovi
	 * plss pogldaj README.txt, a možeš pogledati i online ali prvo predlažem README.txt.
	 */
	
	/*
	 *	Inače ova metoda je tu samo zato što treba inicijalizirati bazu podataka s nekim vrijednostima. Najvjerovatnije će se ova inicijalizacijska metoda
	 *za bazu podataka biti prebačena u paket database ali ovo je bilo najbrže za napisati pa sam ovdje ostavio tu metodu. 
	 */
	public void postConstruct() {
		// Stvorim dvije liste jednu osoba, jednu pasa
		List<Person> persons = List.of(Person.builder().name("Hrvoje").age(20).build());
		List<Dog> dogs = List.of(Dog.builder().name("Spike").age(10).owner(persons.get(0)).build());
		
		// Spremim ih u repositorij
		personRepo.saveAll(persons); 
		dogRepo.saveAll(dogs);
		
		// "Pustim vodu", flush-am repository ako je slučajno stavio vrijednosti negdje sastrane i nije ih pohranio u bazu.
		personRepo.flush();
		dogRepo.flush();
	}
}
