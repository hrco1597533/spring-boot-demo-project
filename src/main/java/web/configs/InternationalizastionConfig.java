package web.configs;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/**
 * <pre>
 * U ovom razredu se konfigurira sve da bi se mogla ostvariti internacionalizacija. Internacionalizacija
 * je mogučnost aplikacije da prilagodi svoj jezik korisniku. Tako sam u ovom projektu napravio za demonstraciju
 * da aplikacija podržava englenski i francuski jezik. To možete isporbati tako da upišete localhost:8080/internacional
 * u browser, naravno kada je aplikacija upaljena.
 * </pre>
 * @author hrvoje
 *
 */

@Configuration
public class InternationalizastionConfig implements WebMvcConfigurer { 
	
	
	/*
	 * Ne bi ulazio previše u ovo, možda kasnije ali uglavnom sve što trebate znati da
	 * smo u mogučnosti pisati aplikacije koje automatski mogu mijenjati jezik. Mogu kasnije
	 * još malo ispričati o tome i istražiti ako se odlučimo za ovo.
	 */
	@Bean
	public LocaleResolver localeResolver() {
		SessionLocaleResolver slr = new  SessionLocaleResolver();
		slr.setDefaultLocale(Locale.ENGLISH);
		return slr;
	}
	
	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
		LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
		lci.setParamName("lang");
		return lci;
	}
	
	@Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:/i18n/messages");
        messageSource.setCacheSeconds(10);
        return messageSource;
    }
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		WebMvcConfigurer.super.addInterceptors(registry);
		
		registry.addInterceptor(localeChangeInterceptor());
	}
}
