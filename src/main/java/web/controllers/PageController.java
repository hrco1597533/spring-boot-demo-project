package web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * <pre>
 * Ovdje ću opisati i @Controller i @RestController anotaciju jer su efektivno jednake. Dakle objekti, ondnosno bean-ovi(ako nezz što je bean
 * vidi u README.txt ili online) su objekti kojima se propagiraju http zahtjevi. Dakle kada netko napiše localhost:8080/getting aktivirat će se
 * doljnja metoda koja se zove gretting. Nekoliko stvari bi vas moglo zbunjivati ovdje, prva, tko daje toj metodi tu referencu na ModelMap. Odgovor
 * je spring. Možete ćak i maknuti taj paramter iz metode ništa se nebi desilo idalje bi sve najnoramlnije radilo(ok osim što ne bi mogli korisiti metodu
 * addAttribute jer onda nemamo referencu). Razmišljajte o tome kao dependency injection-u(vidi README.txt). Ova metod ovisi o ModelMap-u i time što je
 * metoda navela u popisu parametara ModelMap kao svoj dependency dala je spring-u do znanja da joj da objekt koji je tipa ModelMap.
 * Druga stvar koja jako zbuni čovjeka je to da ta metoda vraća String, što je istina ali zapravo i nije na kraju. Ovdje se razlikuju bean-ovi koji su
 * označeni sa @Controller od onih koji su označeni sa @RestController. Kod @Controller-a on će ovaj string tumačiti kao ime .html file-a. Spring boot
 * automatski traži .html file-ove u direktorijima src/main/resources/static i src/main/resources/templates. S druge strane @RestController će te stringove
 * doslovno tretirati kao stringove. Razmišlajte ovako, @Controller polužuje uglavnom statičke resurse, dakle .html, .css, .js ..., dok @RestController
 * poslužuje "manje" "dinamične" resurse kao što su .xml, .json(mi ćemo uglavnom korisiti json, jer je puno bolji za parsiranje u js).
 * </pre>
 * @author hrvoje
 *
 */

@Controller
public class PageController {

	@GetMapping("/gretting")
	public String getting(ModelMap map) {
		
		map.addAttribute("name", "Hrvoje");
		
		return "gretting";
	}
	
	@GetMapping("/international")
	public String getInternationalPage() {
		return "international";
	}
}
