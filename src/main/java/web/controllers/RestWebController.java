package web.controllers;

import java.util.List;
import java.util.stream.StreamSupport;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import database.PersonRepository;
import database.models.Dog;
import database.models.Person;

/**
 * <pre>
 * ################# POGLEDAJ {@link PageController}. ################################
 * </pre>
 * @author hrvoje
 *
 */
@RestController
@RequestMapping("/rest")
public class RestWebController {

	@Autowired
	private PersonRepository personRepository;

	@GetMapping
	public String works() {
		return "Works!";
	}

	@GetMapping("/person/{name}")
	public Person person(@PathVariable(name = "name") String name) {

		return StreamSupport.stream(personRepository.findAll().spliterator(), false).findAny().orElse(
				Person.builder().name("Hrvoje").age(10).dogs(List.of(Dog.builder().name("Spike").age(10).build())).build());
	}

	@PostConstruct
	public void postConstruct() {
		System.out.println("Rest controller constructed");
	}

}
