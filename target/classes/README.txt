Ovo je samo demo projekt koji pokušava približiti spring boot svima, nadam se da će malo pomoći.

Konceptualno projekt je podjeljen na tri dijela:
	1. main 	- ovo je namanji dio, ovo je samo ulazna točka programa koja će kasnije najvjerovatnije
				  biti promjenjena
	2. database - ovaj paket se bavi bazom podataka, definira modele koje sprema u bazu te repositorije
	3. web		- ovaj paket je orjentiran na pružanje web api-ja, uz to pruža i mogučnost internacionalizacije
	
Ovo sve je još uvijek ilustrativno, ali to je nekakva baza na kojoj bi trebali raditi projekt.
Ispod sam probao objasniti kako radi spring boot, preporučam da pročitate tak da me možete pitati kasnije točne
stvari koje vam nisu jasne, međutim ne garantiram da ću znati odgovoriti na njih. U kodu sam Vam ostavio kometare
koji se osvrču na pojedine djelove koda i objašnjavaju što kod radi. Ne znam ni ja sve al želim vam pomoć do razine
da i vi polagano počnete shvaćat o ćemu se radi tak da svi možemo raditi. Sad svakako proučite sve što sam vam ostavio. 





## KAKO RADI SPRING BOOT ##

Pokušat ću objasnit, dakle spring boot inače nije samo ovo što ću vam sad objasnit, inače je puno veći, ali glavna
značajka spring boot-a jest nešto što se zove dependency injection. Probat ću vam ilustrirati kroz primjer, zamislite da imate
razred Stol:

public class Stol {

	private NogeOdStola nogeOdStola;
	private StolnaPloca stolnaPloca;
	
	public Stol(NogeOdStola nogeOdStola, StolnaPolca stolnaPloca) {
		this.nogeOdStola = nogeOdStola;
		this.stolnaPloca = stolnaPloca;
	}
	
	// Getteri, setteri, ostale metode ...
}

Primjetite da bi ste napravili objekt stol su vam potrebna druga dva objekta nogeOdStola i stolnaPloca. Kažemo da Stol zavisi
o objektima NogeOdStola i StolnaPloca, odnosno kažemo da su NogeOdStola i StolnaPloca dependency od Stol-a. I sad ovo nije 
strašno u ovom slučaju ali sad si zamislite da imate neki objekt koji ima jako puno dependency-ja i onda ih sve trebate dati 
kroz konstruktor i pamtiti u članskim varijablama. I sad ovdje spring boot nudi rješenje. Spring boot kaže definiraj mi ti te objekte
NogeOdStola i StolnaPloca i anotiraj ih anotacijom @Component, dakle ovako:

@Component
public class NogeOdStola {
	// ...
}

@Component
public class StolnaPloca {
	// ...
}

I potom napravi razred stol na ovaj način:

@Component
public class Stol {

	@Autowired
	private NogeOdStola nogeOdStola;
	@Autowired
	private StolnaPloca stolnaPloca;
	
	// Getteri, setteri, ostale metode ...
}

Ovime spring boot-u kažemo imam novi objekt koji se zove Stol(anotiran je sa anotacijom @Component) i on zahtjeva dva objekta 
NogeOdStola i StolnaPloca. Ovako ne trebaju vam nikakvi konstruktori ni ništa samo kažete da želite one tamo objekte tu.

E sad kako spring boot to radi?

Zapravo si zamišljajte spring boot kao aplikaciju koja radi iznad naše alikacije. Recimo ako je naša aplikacija kutija, onda je
spring boot kutija u kojoj je naša aplikacija. Svaki razred koji je anotiran sa anotacijom @Component u spring-u se zove bean(grašak).
To nije ništa drugo nego instanca objekta, međutim taj objekt je instanciran i spring ga drži kod sebe u nekakvoj pričuvnoj memoriji.
Anotacijom @Autowired vi kažete spring-u da vam da objekt tipa NogeOdStola u ovu vašu referencu. Dakle spring neće napraviti novu instancu
razreda NogeOdStola, već će vam dati isi objekt kojeg je istancirao prilikom pokretanja aplikacije.

Ako uspijete shvatiti barem malo ovo o čemu pričam bit će vam puno lakše kasnije ali preporučam samostalno učenje. Trebate počet čitat online
tutoriale, što je dosta teško po meni jer je spring boot puno više od ovoga što sam ja tu napisao pa se mnogi tutoriali ne bave uopče s 
osnovama. Preporučam dokumentaciju spring boot-a da se upoznate s platformom te preporučam proučavanje bilo kakvog tutoriala online kao zaseban
projekt, dakle nemojte raditi testove u zajedničkom projektu.



### NAKON ŠTO STE PROČITALI I PROUČILI PROJEKT ###

Da bi mogli pokrenuti program morate imati instaliran posgresql na računalu, a kako smo s tim radili podrazumjevam da svi to imate. Sad vrlo važna
stvar sve postavke se nalaze u src/main/resources/appliaction.properties. Tu morate namjestiti schemu koju će te koristiti, te username i password za
postgresql. Schema vam se postavlja tako da umjesto "postgres" u "spring.datasource.url=jdbc:postgresql://localhost:5432/postgres" stavite kako god se vaša
schema zove. Dakle ako je vasa schema "mojaSchema" napisali bi ste umjesto ovog mojeg "spring.datasource.url=jdbc:postgresql://localhost:5432/mojaSchema".
Mislim da ostalo ne trebate dirat, sada vam samo treba ide u kojemu će te otvoriti ovaj projekt. Ja sam ovo radio u eclipsu ali možete vrlo vjerojatno i
sa intellij-om. Dakako prvo trebate skinut ovaj projekt na svoje računalo i pohraniti ga negdje. Nakon toga ako radite u eclipsu slijedite korake:
	1. stisni desnom tipkom miša na bijelu površinu ProjectExplorer-a(to vam je tamo gdje vidite sve svoje projekte inače)
	2. odite na import i onda upišite u "tražilicu" "Existing Maven Projects" i stisnite next
	3. odite na browse i navigirajte do direktorija gdje se nalazi projekt na vašem računalu
	4. morate biti baš u direktoriju projekta, to će te znati ako vidite da u tom direktoriju postoji pom.xml file
	5. nakon toga stisnite finish
Moguće je da će onda neko duže vrijeme eclipse u pozadini skidati maven dependency-je al postite ga da odvrti. Nakon toga odite u paket main u
razred DemoApplication i pokrenite taj main kojeg vidite tamo. Dakle desna tipka miša iznad main metode i "Run as" -> "Java application". Nakon 
toga trebala bi se pokrenuti aplikacija i onda možete probati otići u svoj browser i upisati localhost:8080. Trebalo bi preusmjeriti na login page.
Login page zahtjeva username i password. Namjestio sam da je username i password "feta". Isprobajte slijedeć url-ove:
	localhost:8080/index
	localhost:8080/gretting
	localhost:8080/international
	localhost:8080/rest
	localhost:8080/rest/person/Pero
	

### MOGUĆE POTEŠKOĆE ###
Projekt podrazumjeva da imate instaliran posgresql i da imate javu11. Sa postgresql-om vam ne mogu puno pomoć ovako preko poruka, al možda mogu malo sa java11.
Dakle morate otići na https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html i skinuti jdk11 ovisno o operativnom sustavu.
Nakon što ste skinuli jdk11 trebate otići u postavke u eclipsu, odete u demo projekt i vidjet ćete da piše pod jednom od stavkama "JRE System Library", desnu tipku
miša na to i "Properties". Tamo će vam ponuditi padajući izbornik sa "Enviromenti-ma" tamo bi trebao pisati negdje nešto tipa "JDK11" ili sl. onda to odaberite i to 
bi trebalo biti to ako je problem do jave.

Naravno može biti još pun kufer stvari al nemrem to ovak rješavat ak treba netko neš nek se javi.


